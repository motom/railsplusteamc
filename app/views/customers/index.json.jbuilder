json.array!(@customers) do |customer|
  json.extract! customer, :id, :employeename, :customername, :memo, :visited
  json.url customer_url(customer, format: :json)
end
