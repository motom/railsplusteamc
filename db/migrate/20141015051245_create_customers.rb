class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :employeename
      t.string :customername
      t.text :memo
      t.date :visited

      t.timestamps
    end
  end
end
